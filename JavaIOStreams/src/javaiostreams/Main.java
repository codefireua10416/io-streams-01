/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaiostreams;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        baos.write(97);
        baos.write(98);
        baos.write(99);
        baos.write(100);
        baos.write(101);
        baos.write(102);

        byte[] toByteArray = baos.toByteArray();
        String arrayString = Arrays.toString(toByteArray);

        System.out.println(arrayString);

        String line = new String(toByteArray);

        System.out.println(line);

        ByteArrayInputStream bais = new ByteArrayInputStream(toByteArray);

//        while (bais.available() > 0) {
//            System.out.println(bais.read());
//        }
//        int available = bais.available();
        byte[] buffer = new byte[32];

        int offset = 9;
        int read = bais.read(buffer, offset, bais.available());

        System.out.println(new String(buffer, offset, read));

    }

}
