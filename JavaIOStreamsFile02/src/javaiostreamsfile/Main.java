/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaiostreamsfile;

import java.io.File;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File project_dir = new File("." + File.separator);
        
        // If directory empty or object is a file, return null.
        String[] list = project_dir.list();
        
        if (list != null) {
            for (String sub : list) {
                System.out.println(sub);
            }
        }
        
        System.out.println("\nROOTS:");
        File[] roots = File.listRoots();
        
        for (File root : roots) {
            System.out.println(root.getAbsolutePath());
            String[] subRoot = root.list();
            
            for (String sub : subRoot) {
                System.out.println("  "+sub);
            }
        }
    }
    
}
