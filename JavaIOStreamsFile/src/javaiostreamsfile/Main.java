/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaiostreamsfile;

import java.io.File;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // :: PATHS ::
        // -----------------------------------
        // Absolute:
        // ---------
        // Win:  C:\\Users\\human\\Downloads\\
        // UNIX: /Users/human/Downloads/
        // -----------------------------------
        // Relative:
        // ---------
        // ALL:  file.txt
        // Win:  .\\file.txt
        // UNIX: ./file.txt
        //////////////////////////////////////
        
        File file = new File("build.xml");
        System.out.println("build.xml: " + file.exists());
        
        File dir = new File("dist");
        System.out.println("dist: " + dir.exists());
        
    }
    
}
